from sqlalchemy import Column, Integer, String, Text, ForeignKey, DateTime, Date, Boolean
from sqlalchemy.orm import backref, relationship
from .meta import Base
from flask_bcrypt import Bcrypt
import datetime

flask_bcrypt = Bcrypt()


class User(Base):
    """ User Table for storing user related details """
    __tablename__ = "user"

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(255), unique=True, nullable=False)
    username = Column(String(50), nullable=False)
    password_hash = Column(String(100), nullable=False)
    contact_number = Column(String(10), nullable=False, unique=True)

    varified = Column(Boolean, nullable=False, default=False)

    registered_on = Column(DateTime, nullable=False)
    last_updated_on = Column(DateTime, nullable=False)

    # Relationships
    booking = relationship("Booking", back_populates="user")

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(
            password).decode('utf-8')

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<User '{}'>".format(self.username)


class Booking(Base):
    """ Booking Table to store booking details """
    __tablename__ = "booking"

    id = Column(Integer, primary_key=True, autoincrement=True)
    booking_date = Column(Date, nullable=False, default=datetime.datetime.utcnow())
    checkin = Column(Date, nullable=False)
    checkout = Column(Date, nullable=False)
    status = Column(String(50))

    room_id = Column(Integer, ForeignKey('room.id'), nullable=False)
    room = relationship("Room", back_populates='booking')
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship("User", back_populates='booking')

    # Realationship
    notification = relationship("Notification", back_populates="booking")
    payment = relationship("Payment", back_populates="booking")


class Notification(Base):
    """ Notiication table for keep tracking user notifications """
    __tablename__ = 'notification'

    id = Column(Integer, primary_key=True)
    type = Column(String(100), nullable=False)
    detail = Column(Text, nullable=False)
    datetime = Column(DateTime, nullable=False)

    booking_id  = Column(Integer, ForeignKey('booking.id'), nullable=False)
    booking = relationship("Booking", back_populates="notification")
