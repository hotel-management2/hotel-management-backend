from sqlalchemy.orm import relationship
from sqlalchemy.sql.sqltypes import Boolean, DateTime
from .meta import Base
from sqlalchemy import Column, Integer, String, ForeignKey

class Payment(Base):
    """To store the transaction related to the payment of booking."""
    __tablename__ = "payment"

    id = Column(Integer, primary_key=True, autoincrement=True)
    txnid = Column(String(30), unique=True)
    status = Column(String, nullable=False)
    amount = Column(Integer, nullable=False)
    online = Column(Boolean, nullable=False)
    txn_date = Column(DateTime, nullable=False)

    booking_id = Column(Integer, ForeignKey('booking.id'), nullable=False)
    booking = relationship("Booking", back_populates="payment")

    def __repr__(self):
        txndetail = {
            "txnid": self.txnid,
            "status": self.status,
            "amount": self.amount,
            "method": self.method
        }