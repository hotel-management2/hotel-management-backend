from .meta import Base
from .employeeModel import Employee
from .userModel import User, Booking, Notification
from .roomModel import Room, RoomDetail
from .paymentModel import Payment
from .otpModel import Otp
from .migratorModel import Migrator


def createTables(engine):
    # print("Binding")
    print(Base.metadata.tables.keys())
    Base.metadata.bind = engine
    # print("binding done")
    Base.metadata.create_all(engine)


def destroyTables(engine):
    Base.metadata.drop_all(engine)
