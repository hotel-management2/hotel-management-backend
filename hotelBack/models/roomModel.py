from sqlalchemy import Column, Integer, String, Text, ForeignKey
from sqlalchemy.orm import relationship
from .meta import Base

class Room(Base):
    """For storing the room details. """
    __tablename__ = "room"

    id = Column(Integer, primary_key=True, autoincrement=True)
    room_number = Column(Integer, nullable=False)
    description = Column(Text, nullable=False)
    cost = Column(Integer, nullable=False)

    room_detail_id = Column(Integer, ForeignKey('room_detail.id'), nullable=False)
    room_detail = relationship("RoomDetail", back_populates="room")
    booking = relationship("Booking", back_populates="room")


class RoomDetail(Base):
    """For storing room details and feedbacks."""
    __tablename__ = "room_detail"

    id = Column(Integer, primary_key=True, autoincrement=True)
    capacity = Column(Integer, nullable=False)
    room_type = Column(String(100), nullable=False)
    room_img = Column(String(100))
    description = Column(Text)

    room = relationship("Room", back_populates="room_detail")
