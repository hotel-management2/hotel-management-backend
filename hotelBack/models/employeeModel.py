from sqlalchemy import Column, Integer, String, DateTime, Boolean
from .meta import Base
from flask_bcrypt import Bcrypt

flask_bcrypt = Bcrypt()


class Employee(Base):
    """To store detail of employees"""
    __tablename__ = "employee"

    id = Column(Integer, primary_key=True, autoincrement=True)
    email = Column(String(255), unique=True, nullable=False)
    username = Column(String(50), nullable=False)
    password_hash = Column(String(100), nullable=False)
    contact_number = Column(String(10), nullable=False)

    varified = Column(Boolean, nullable=False, default=False)
    admin = Column(Boolean, nullable=False, default=False)

    registered_on = Column(DateTime, nullable=False)
    last_updated_on = Column(DateTime, nullable=False)

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(
            password).decode('utf-8')

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def __repr__(self):
        return "<Employee '{}'>".format(self.username)
