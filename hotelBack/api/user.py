from flask import Blueprint, request
import datetime

from hotelBack.response.response import make_response
from hotelBack.response.statusCodes import HTTPStatus
from hotelBack.models import User
from hotelBack.constant import Constants
from hotelBack.auth.otpAuth import generateOtp, generateOtpToken
from hotelBack.auth import get_token, validate_token

userBP = Blueprint('userApi', __name__)


@userBP.route('/register', methods=['POST'])
def useraction():
    if request.method == 'POST':
        data = request.json
        return save_new_user(data=data)
    else:
        return make_response("method not allowed", HTTPStatus.MethodNotAllowed)


def save_new_user(data):
    required_parameters = ['email', 'name', 'password', 'contact_number']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    from server import SQLSession
    session = SQLSession()
    connection = session.connection()

    _usr = session.query(User).filter_by(contact_number=data['contact_number']).first()
    if _usr:
        session.close()
        connection.close()
        return make_response("Contact already exist.", HTTPStatus.BadRequest)

    user = session.query(User).filter_by(email=data['email']).first()
    if not user:

        new_user = User(
            email=data['email'],
            username=data['name'].strip(),
            password=data['password'],
            contact_number=data['contact_number'],
            registered_on=datetime.datetime.utcnow(),
            last_updated_on=datetime.datetime.utcnow()
        )
        otp = generateOtp(data['email'])
        otp_token = generateOtpToken(data['email'])

        # uncomment below two lines for sending otp on mail
        # if not send_confirmation_mail(data['name'], otp, data['email']):
        #     return make_response("Err sending mail, check your email id", HTTPStatus.InternalError)

        try:
            session.add(new_user)
            session.commit()
            session.close()
            connection.close()
            payload = {
                "token": otp_token,
                "email": data['email']
            }
            return make_response("User created successfully. otp sent to email", HTTPStatus.Success, payload)
        except Exception as e:
            session.close()
            connection.close()
            return make_response("Problem saving in db", HTTPStatus.InternalError)
    else:
        session.close()
        connection.close()
        return make_response("User already exists", HTTPStatus.BadRequest)


@userBP.route('/login', methods=['POST'])
def login():
    data = request.json
    token = get_token(data)
    required_parameters = ['email', 'password']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    if token == Constants.NoUser:
        return make_response("User not exist", HTTPStatus.NotFound)
    elif token == Constants.NotVerified:
        return make_response("User not verified/blocked.", HTTPStatus.NotVerified)
    elif token == Constants.InvalidCredential:
        return make_response("Invalid Credential", HTTPStatus.UnAuthorised)
    else:
        res = {
            "email": data['email'],
            "auth-token": token
        }
        return make_response(msg="Token success", status_code=HTTPStatus.Success, payload=res)


@userBP.route('/', methods=['GET'])
def get_my_detail():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)

    valid_token, usr_ = validate_token(auth_token)
    if valid_token:
        result = {
            "name": usr_.username,
            "varified": usr_.varified,
            "contact_number": usr_.contact_number,
            "email": usr_.email,
        }
        return make_response(msg="Detail of the user", status_code=HTTPStatus.Success, payload=result)

    else:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("User not verified", HTTPStatus.UnAuthorised)


@userBP.route('/change_password', methods=['POST'])
def change_password():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)
    valid_token, usr_ = validate_token(auth_token)
    if valid_token:
        data = request.json
        from server import SQLSession
        session = SQLSession()
        required_parameters = ['old_password', 'new_password']
        if (set(required_parameters)-data.keys()):
            return make_response("Missing Input.", HTTPStatus.BadRequest)

        connection = session.connection()
        u = session.query(User).filter_by(email=usr_.email).first()
        if u.check_password(data.get('old_password')):
            u.password = data['new_password']
            try:
                session.commit()
                session.close()
                connection.close()
                return make_response("Password change successful", HTTPStatus.Success)
            except:
                session.close()
                connection.close()
                return make_response("DB error", HTTPStatus.InternalError)
            finally:
                session.close()
                connection.close()
        else:
            session.close()
            connection.close()
            return make_response("Old password does not matched.", HTTPStatus.UnAuthorised)
    else:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)


@userBP.route('/update_profile', methods=['POST'])
def update_profile():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)
    valid_token, usr_ = validate_token(auth_token)
    if valid_token:
        from server import SQLSession
        session = SQLSession()
        connection = session.connection()
        data = request.json
        u = session.query(User).filter_by(email=usr_.email).first()
        # update the values if key to update is passed or leave it as same
        u.username = data['name'] if 'name' in data else u.username
        try:
            session.commit()
            session.close()
            connection.close()
            return make_response("Profile update successful", HTTPStatus.Success)
        except:
            session.close()
            connection.close()
            return make_response("DB error", HTTPStatus.InternalError)
        finally:
            session.close()
            connection.close()
    else:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
