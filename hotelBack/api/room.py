import datetime
from flask import Blueprint, request
from sqlalchemy import or_

from hotelBack.response.response import make_response
from hotelBack.response.statusCodes import HTTPStatus
from hotelBack.models import Booking, Room, RoomDetail
from hotelBack.auth import validate_token
from hotelBack.constant import Constants

roomBP = Blueprint('roomApi', __name__)


@roomBP.route('/', methods=['POST'])
def get_rooms():
    data = request.json
    required_parameters = ['check_in', 'check_out']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    from server import SQLSession
    session = SQLSession()
    connection = session.connection()

    cin=data['check_in']
    cout=data['check_out']
    booked_rooms = session.query(Booking).filter(
        or_(
            Booking.checkout.between(cin, cout),
            Booking.checkin.between(cin, cout))
            ).all()
    if ('capacity' in data) and ('category' in data):
        all_rooms = session.query(Room).join(RoomDetail).filter(
            RoomDetail.capacity==data['capacity']).filter(
                RoomDetail.room_type==data['category']).all()
    elif 'capacity' in data:
        all_rooms = session.query(Room).join(RoomDetail).filter(RoomDetail.capacity==data['capacity']).all()
    elif 'category' in data:
        all_rooms = session.query(Room).join(RoomDetail).filter(RoomDetail.room_type==data['category']).all()
    else:
        pass

    booked_rooms_id = []
    print(booked_rooms)
    for i in booked_rooms:
        booked_rooms_id.append(i.room.id)

    resp = []
    for i in all_rooms:
        if i.id in booked_rooms_id:
            continue
        rm = {
            "id": i.id,
            "room_number": i.room_number,
            "cost": i.cost,
            "room_category": i.room_detail.room_type,
            "capacity": i.room_detail.capacity,
            "room_img": i.room_detail.room_img,
            "description": i.room_detail.description
        }
        resp.append(rm)

    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, resp)


@roomBP.route('/book_room', methods=['POST'])
def book_room():
    auth_token = request.headers.get('auth-token')
    if not auth_token:
        return make_response("No 'auth-token' in header", HTTPStatus.NoToken)

    valid_token, usr_ = validate_token(auth_token)
    if not valid_token:
        if usr_ == Constants.InvalidToken:
            return make_response("Invalid token", HTTPStatus.InvalidToken)
        elif usr_ == Constants.TokenExpired:
            return make_response("Token expired.", HTTPStatus.InvalidToken)
        else:
            return make_response("User not verified", HTTPStatus.UnAuthorised)
    data = request.json
    required_parameters = ['check_in', 'check_out', 'room_id']
    if (set(required_parameters)-data.keys()):
        return make_response("Missing Input.", HTTPStatus.BadRequest)

    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    
    cin=data['check_in']
    cout=data['check_out']
    booked_rooms = session.query(Booking).filter(
        or_(
            Booking.checkout.between(cin, cout),
            Booking.checkin.between(cin, cout))
            ).all()
    booked_rooms_id = []
    print(booked_rooms)
    for i in booked_rooms:
        booked_rooms_id.append(i.room.id)
    if data['room_id'] in booked_rooms:
        session.close()
        connection.close()
        return make_response("Room already Booked", HTTPStatus.BadRequest)
    
    book = Booking(
        booking_date=datetime.datetime.utcnow(),
        checkin=cin,
        checkout=cout,
        status=Constants.Booked,
        room_id=data['room_id'],
        user_id=usr_.id
    )
    session.add(book)
    session.commit()

    booking = session.query(Booking).order_by(Booking.id.desc()).first()
    payload = {
        "id": booking.id,
        "booking_date": booking.booking_date,
        "check_in": booking.checkin,
        "check_out": booking.checkout,
        "status": booking.status,
        "room_id": booking.room_id,
        "user_id": booking.room_id
    }

    session.close()
    connection.close()
    return make_response("success", HTTPStatus.Success, payload)
