import os
from dotenv import load_dotenv
import datetime
load_dotenv(os.environ.get('DOTENV'))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY')
    DEBUG = False


class DbEngine_config():
    DB_DIALECT = os.environ.get('DB_DIALECT') or 'postgresql'
    DB_HOST = os.environ.get('DB_HOST') or 'localhost'
    DB_PORT = os.environ.get('DB_PORT') or '5432'
    DB_USER = os.environ.get('DB_USER') or 'postgres'
    DB_PASS = os.environ.get('DB_PASS') or 'postgres'
    DB_NAME = os.environ.get('DB_NAME') or 'hotel_management'
    DB_URL = f'{DB_DIALECT}://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}'
    SQLALCHEMY_DATABASE_URI = DB_URL
    print(DB_URL)


class MailConfig():
    MAIL_SERVER = 'smtpout.secureserver.net'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_DEBUG = True
    MAIL_USERNAME = os.environ.get('MAIL_USER')
    MAIL_PASSWORD = os.environ.get('MAIL_PASS')
    MAIL_DEFAULT_SENDER = ("Hotel_SK Team", os.environ.get('MAIL_USER'))
    MAIL_MAX_EMAILS = None
    # MAIL_SUPPRESS_SEND = ''
    MAIL_ASCII_ATTACHMENTS = False


Token_Validity = 1
RESET_PASSWORD_TIMEOUT_MINUTES = 3
tzone = datetime.timedelta(hours=5) + datetime.timedelta(minutes=30)


class ProductionConfig(Config):
    DEBUG = False
