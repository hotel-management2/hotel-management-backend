from flask_sqlalchemy import BaseQuery

from hotelBack.constant import Constants


def paginate_query(sa_query, page, per_page=Constants.PerPage, error_out=True):
    sa_query.__class__ = BaseQuery
    return sa_query.paginate(page, per_page, error_out)
