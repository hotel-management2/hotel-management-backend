import pyotp
from hotelBack.constant import Constants
from hotelBack.models import Otp
import datetime
import jwt
import os


def generateOtp(email):
    secret = pyotp.random_base32()
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    totp = pyotp.TOTP(secret, interval=Constants.OTP_EXPIRY)
    o_ = session.query(Otp).filter_by(email=email).first()
    if not o_:
        try:
            otp = totp.now()
            o = Otp(
                email=email,
                otp=otp,
                secret=secret
            )
            session.add(o)
            session.commit()
            session.close()
            connection.close()
            return otp
        except:
            session.close()
            connection.close()
            return None
    else:
        # update otp
        otp = totp.now()
        o_.otp = otp
        o_.secret = secret
        o_.generated_time_stamp = datetime.datetime.now()
        o_.isValid = True
        try:
            session.commit()
            session.close()
            connection.close()
            return otp
        except:
            session.close()
            connection.close()
            return None


def generateOtpToken(email):
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    user = session.query(Otp).filter_by(email=email).first()
    session.close()
    connection.close()
    if not user:
        return Constants.NoUser
    else:
        return jwt.encode({'email': user.email, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=Constants.OTP_TOKEN_EXPIRY)}, os.environ.get('SECRET_KEY_OTP')).decode('UTF-8')


def verifyOtpToken(token):
    try:
        from server import SQLSession
        session = SQLSession()
        conn = session.connection()
        try:
            d = jwt.decode(token, os.environ.get('SECRET_KEY_OTP'))
            usr = d['email']
            user_ = session.query(Otp).filter_by(email=usr).first()
        except Exception as e:
            session.close()
            conn.close()

        session.close()
        conn.close()
        if not user_:
            return False, Constants.InvalidToken
        else:
            return True, user_
    except jwt.ExpiredSignatureError:
        return False, Constants.TokenExpired


def verifyOtp(email, otp):
    from server import SQLSession
    session = SQLSession()
    connection = session.connection()
    o_ = session.query(Otp).filter_by(email=email).first()

    if not o_:
        session.close()
        connection.close()
        return False
    if not o_.isValid:
        session.close()
        connection.close()
        return False
    totp = pyotp.TOTP(o_.secret, interval=Constants.OTP_EXPIRY)
    verified = totp.verify(otp)
    if verified:
        o_.isValid = False
        session.delete(o_)
        session.commit()
        session.close()
        connection.close()
        return True
    else:
        session.close()
        connection.close()
        return False
