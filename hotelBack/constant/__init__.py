class Constants:
    # User action constants
    NoUser = "NoSuchUSer"
    NotVerified = "NotVerifiedYet"
    InvalidCredential = "InvalidCredentials"
    # Token action constants
    InvalidToken = "InvalidToken"
    TokenExpired = "ExpiredToken"

    #Room Status
    Booked = "Booked"

    # admin
    NotAdmin = "NotAdmin"

    # OTP expiry in seconds
    OTP_EXPIRY = 600

    # Otp token expiry time in minutes
    OTP_TOKEN_EXPIRY = 10

    # paginate concept parameters
    PerPage = 21
