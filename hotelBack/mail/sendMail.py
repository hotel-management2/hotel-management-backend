from flask_mail import Message
from flask import render_template


def send_confirmation_mail(user, otp, email):
    from server import mail
    msg = Message(
        subject="Registration (Hotel_SK).",
        recipients=[email]
    )
    msg.html = render_template('confirmRegistration.html', user=user, otp=otp)
    try:
        print("Sending to:: ", email)
        mail.send(msg)
        return True
    except:
        return False


def send_forget_password_mail(user, otp, email):
    from server import mail
    msg = Message(
        subject="Reset Password (Hotel_SK).",
        recipients=[email]
    )
    msg.html = render_template('reset_pass.html', user=user, otp=otp)
    try:
        mail.send(msg)
        return True
    except:
        return False
