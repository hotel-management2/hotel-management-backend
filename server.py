from hotelBack.api import *
from hotelBack import create_db_engine, create_db_sessionFactory
from hotelBack.config import DbEngine_config, MailConfig
from flask_cors import CORS
from flask import Flask
from flask_mail import Mail
import optparse
import os
from dotenv import load_dotenv

parser = optparse.OptionParser()
parser.add_option('-e', '--env', dest='envpath', help='Path of .env file to load.')
(options, arguments) = parser.parse_args()
os.environ['ENV'] = options.envpath if options.envpath else '.env'
load_dotenv(os.environ.get('DOTENV'))


engine = create_db_engine(DbEngine_config)
SQLSession = create_db_sessionFactory(engine)

app = Flask(__name__)
app.config.from_object(MailConfig())
app.config.from_object(DbEngine_config())

# Initilaize mailer
mail = Mail()
mail.init_app(app)

CORS(app, supports_credentials=True)


@app.route('/')
def get():
    return "<h1> Hello, Welcome to backend of Hotel-management-system </h1>"


# V1 APIs
app.register_blueprint(userBP, url_prefix='/{}/user'.format(API_VERSION_V1))
app.register_blueprint(roomBP, url_prefix='/{}/room'.format(API_VERSION_V1))


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
