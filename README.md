# Hotel-Management

## Backend app for hotel-management

## Setup instructions

### Server

- #### Step 1 (optional but recommended)

  Create a python virtual environment by using virtualenv or conda

  ```bash
  conda create -n environment python3.6
  ```

  or

  ```bash
  python -m venv environment && source venv/bin/activate
  ```

- #### Step 2

  Clone this repo

  ```bash
  git clone <repo> && cd <name>
  ```

- #### Step 3

  Install dependencies

  ```bash
  pip install -r requirements.txt
  ```
