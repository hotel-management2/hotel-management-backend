# DataBase CLI

**To set password for operation i used .env file**

**Step 1:**

    PASS=<Your-Password>

**Step 2:** Create Migration directory to store migration file.

        $ python cli_database.py init

NOTE: In migrations/env.py add compare_type=True for extra comparision of schema

--------

## Now lets run our first migration


**Step1:** Creating first migration script

>   **If database already exist:**
>       
>       $ python cli_database.py revision

>    **If creating new database:**
>
>       $ python cli_database.py migrate <Your-message>

**Step 2:** Upgrade the database

    $ python cli_database.py upgrade <Your-message> -r <revision_identifier(default=head)>

---------

## To downgrade database to last version

    $ python cli_database.py downgrade <Your-message>

--------
    